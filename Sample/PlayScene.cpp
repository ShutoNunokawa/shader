#include "PlayScene.h"
#include "MyGameEngine\Quad.h"
#include "MyGameEngine\Camera.h"
#include "MyGameEngine\Light.h"


//-----------------------------------------
//機能 : コンストラクタ
//引数 : なし
//戻値 : なし
//-----------------------------------------
PlayScene::PlayScene()
{
	_angle = 0;

}

//-----------------------------------------
//機能 : デストラクタ
//引数 : なし
//戻値 : なし
//-----------------------------------------
PlayScene::~PlayScene()
{
	
}

//-----------------------------------------
//機能 : 初期化処理
//引数 : なし
//戻値 : なし
//-----------------------------------------
void PlayScene::Init()
{

	_torus = Fbx::Create("Assets\\Torus.fbx");
	AddChild(_torus);
	
	//ライト
	auto light = Light::Create();
	this->AddChild(light);

}


//-----------------------------------------
//機能 : 更新処理
//引数 : なし
//戻値 : なし
//-----------------------------------------
void PlayScene::Update()
{


	_angle += 2;			//回転速度
	//_fbx->SetRotate(_angle, 0, 0);		//縦回転
	_torus->SetRotate(0, _angle, 0);	//横回転(駒的な)
	//_fbx->SetRotate(-1, _angle/_angle, _angle+_angle);	//横回転


}