#include "Fbx.h"
#include "Global.h"
#include "Camera.h"

Fbx::Fbx()
{
	_vertexBuffer = nullptr;		//ポインタの初期化
	_indexBuffer = nullptr;			//ポインタの初期化
	_pTexture = nullptr;			//ポインタの初期化


	 _manager = nullptr;
	_importer = nullptr;
	_scene = nullptr;

	_polygonCountOfMaterial = nullptr;

	_vertexCount = 0;
	_materialCount = 0;
	_indexCount = 0;

	_effect = nullptr;
}


Fbx::~Fbx()
{
	_manager->Destroy();	//これを呼ぶことで全て解放される
	SAFE_DELETE(_material);

	SAFE_RELEASE(_vertexBuffer);	//安全な解放
	//SAFE_RELEASE(_pTexture);		//安全な解放
	delete[] _pTexture;

	//ひとつひとつReleaseした後配列をdelete
	for (int i = 0; i < _materialCount; i++)
	{
		_indexBuffer[i]->Release();		
		SAFE_DELETE(_polygonCountOfMaterial);
	}
	delete[] _indexBuffer;

	SAFE_RELEASE(_effect);
}

Fbx* Fbx::Create(LPCSTR fileName)
{
	auto fbx = new Fbx();
	fbx->Load(fileName);
	return fbx;
}

void Fbx::Load(LPCSTR fileName)
{

	_manager = FbxManager::Create();
	_importer = FbxImporter::Create(_manager, "");
	_scene = FbxScene::Create(_manager, "");

	_importer->Initialize(fileName);	//インポーターでFBXファイルを開く
	_importer->Import(_scene);			//開いたファイルをシーンに渡す
	_importer->Destroy();				//インポーターの解放


	char defaultCurrentDir[MAX_PATH];
	GetCurrentDirectory(MAX_PATH, defaultCurrentDir);	//現在のディレクトリを調べる

	char dir[MAX_PATH];
	_splitpath_s(fileName, NULL, 0, dir, MAX_PATH, NULL, 0, NULL, 0);	//ファイル名からフォルダ名を取り出す
	SetCurrentDirectory(dir);	//カレントディレクトリ(今見ている場所)がAssetsの中になる


	//FBXファイルは階層構造になっている	一つ一つのデータのことをノード、一番上にあるノードをルートノード
	//FBXを読み込んだシーンからルートノードを取り出す
	FbxNode* rootNode = _scene->GetRootNode();

	//ルートノードの子が何人いるか調べる
	int childCount = rootNode->GetChildCount();

	//子の数だけループ
	for (int i = 0; childCount > i; i++)
	{
		//ノードの内容をチェック
		CheckNode(rootNode->GetChild(i));	//子の上から順にノード情報を渡す
	}
	SetCurrentDirectory(defaultCurrentDir);

	LPD3DXBUFFER err = 0;				

	if (FAILED(D3DXCreateEffectFromFile(g.pDevice,
		"TextureShader.hlsl", NULL, NULL,
		NULL, NULL, &_effect, &err)))
	{
		MessageBox(NULL, (char*)err->GetBufferPointer(), "シェーダーエラー", MB_OK);
	}

}

void Fbx::Draw()
{

	//表示したい頂点バッファとインデックスバッファを指定
	g.pDevice->SetStreamSource(0, _vertexBuffer, 0, sizeof(Vertex));
	//g.pDevice->SetIndices(_indexBuffer);
	//g.pDevice->SetMaterial(&_material);
	//g.pDevice->SetTexture(0, _pTexture);

	g.pDevice->SetFVF(D3DFVF_XYZ | D3DFVF_NORMAL | D3DFVF_TEX1);	//頂点の所持情報



	//移動行列
	D3DXMATRIX trans;
	D3DXMatrixTranslation(&trans, _position.x, _position.y, _position.z);

	//回転行列
	D3DXMATRIX rotateX, rotateY, rotateZ;
	D3DXMatrixRotationX(&rotateX, D3DXToRadian(_rotate.x));
	D3DXMatrixRotationY(&rotateY, D3DXToRadian(_rotate.y));
	D3DXMatrixRotationZ(&rotateZ, D3DXToRadian(_rotate.z));
	//_effect->SetMatrix("matRotateX", &rotateX);
	//_effect->SetMatrix("matRotateY", &rotateY);
	//_effect->SetMatrix("matRotateZ", &rotateZ);

	//回転行列
	D3DXMATRIX rotate = rotateZ * rotateX * rotateY;
	_effect->SetMatrix("matRotate", &rotate);

	//拡大縮小
	D3DXMATRIX scale;
	D3DXMatrixScaling(&scale, _scale.x, _scale.y, _scale.z);

	//ワールド行列
	D3DXMATRIX world = scale * rotateZ * rotateX * rotateY * trans;
	g.pDevice->SetTransform(D3DTS_WORLD, &world);	//行列セット
	_effect->SetMatrix("matWorld", &world);			//ワールド行列を適用

	//ビュー行列
	D3DXMATRIX view;
	g.pDevice->GetTransform(D3DTS_VIEW, &view);
	_effect->SetMatrix("matView", &view);

	//プロジェクション行列
	D3DXMATRIX proj;
	g.pDevice->GetTransform(D3DTS_PROJECTION, &proj);
	_effect->SetMatrix("matProj", &proj);

	//視点情報を渡す
	//_effect->SetVector("vecCam", &D3DXVECTOR4(0, 3, -5, 0));
	_effect->SetVector("vecCam", (D3DXVECTOR4*)&g.pCamera->GetPosition());

	D3DLIGHT9 lightDir;
	g.pDevice->GetLight(0, &lightDir);
	_effect->SetVector("vecLightDir", (D3DXVECTOR4*)&lightDir.Direction);

	//D3DXVECTOR4 c = g.pCamera->GetPosition();
	//c.w = 0;
	//_effect->SetVector("vecCam", (D3DXVECTOR4*)&c);

	_effect->Begin(NULL, 0);
	_effect->BeginPass(0);

	for (int i = 0; i < _materialCount; i++)
	{
		g.pDevice->SetIndices(_indexBuffer[i]);
		g.pDevice->SetMaterial(&_material[i]);
		g.pDevice->SetTexture(0, _pTexture[i]);
		g.pDevice->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0, _vertexCount, 0, _polygonCountOfMaterial[i]);	//4は頂点の数	2はポリゴン数
	}
	_effect->EndPass();
	_effect->End();
}



void Fbx::CheckNode(FbxNode* pNode)
{
	//受け取ったノードの属性を取得し、メッシュ情報を調べる
	FbxNodeAttribute* attr = pNode->GetNodeAttribute();
	if (attr->GetAttributeType() == FbxNodeAttribute::eMesh)
	{
		//メッシュノードだった
		_materialCount = pNode->GetMaterialCount();
		_material = new D3DMATERIAL9[_materialCount];		//マテリアルの数を入れる変数を配列に
		_pTexture = new LPDIRECT3DTEXTURE9[_materialCount];	//テクスチャを入れる変数を配列に

		//マテリアル情報読み込み
		for (int i = 0; i < _materialCount; i++)
		{
			FbxSurfaceLambert* lambert = (FbxSurfaceLambert*)pNode->GetMaterial(i);
			FbxDouble3 diffuse = lambert->Diffuse;	//拡散反射光取得
			FbxDouble3 ambient = lambert->Ambient;	//環境光取得

			//拡散反射光と環境光をD3DMATERIAL9型の配列に入れる
			ZeroMemory(&_material[i], sizeof(D3DMATERIAL9));

			_material[i].Diffuse.r = (float)diffuse[0];
			_material[i].Diffuse.g = (float)diffuse[1];
			_material[i].Diffuse.b = (float)diffuse[2];
			_material[i].Diffuse.a = 1.0f;

			_material[i].Ambient.r = (float)ambient[0];
			_material[i].Ambient.g = (float)ambient[1];
			_material[i].Ambient.b = (float)ambient[2];
			_material[i].Ambient.a = 1.0f;

			FbxProperty lProperty = pNode->GetMaterial(i)->FindProperty(FbxSurfaceMaterial::sDiffuse);	//設定されたテクスチャの情報を取得
			FbxFileTexture* textureFile = lProperty.GetSrcObject<FbxFileTexture>(0);		//使われたファイルの情報を取得

			if (textureFile == NULL)
			{
				_pTexture[i] = NULL;
			}
			else
			{
				const char* textureFileName = textureFile->GetFileName();	//ファイル名取得
				
				char name[_MAX_FNAME];		//ファイル名
				char ext[_MAX_EXT];		//拡張子
				_splitpath_s(textureFileName, NULL, 0, NULL, 0, name, _MAX_FNAME, ext, _MAX_EXT);
				wsprintf(name, "%s%s", name, ext);	//ファイル名と拡張子を結合させて表示する
				
				//ファイルを読み込みテクスチャを作成
				D3DXCreateTextureFromFileEx(g.pDevice, name, 0, 0, 0, 0, D3DFMT_UNKNOWN, D3DPOOL_DEFAULT,
					D3DX_FILTER_NONE, D3DX_DEFAULT, NULL, NULL, NULL, &_pTexture[i]);

			}
		}

		CheckMesh(pNode->GetMesh());
	}
	else
	{
		//メッシュ以外のデータだった
		int childCount = pNode->GetChildCount();
		for (int i = 0; childCount > i; i++)
		{
			CheckNode(pNode->GetChild(i));		//再帰呼び出し
		}
	}

}

void Fbx::CheckMesh(FbxMesh* pMesh)
{
	//頂点情報を読み込む
	//pVertexPos [0][0]には最初の頂点のX座標
	//pVertexPos [0][1]には最初の頂点のY座標
	//pVertexPos [0][2]には最初の頂点のZ座標
	//pVertexPos [1][0]には2番目の頂点のX座標が入る
	FbxVector4* pVertexPos = pMesh->GetControlPoints();
	_vertexCount = pMesh->GetControlPointsCount();
	_polygonCount = pMesh->GetPolygonCount();
	_indexCount = pMesh->GetPolygonVertexCount();
	Vertex* vertexList = new Vertex[_vertexCount];	//頂点の数と同じサイズのVertex構造体型の配列

	//posにデータをコピーする
	for (int i = 0; _vertexCount > i; i++)
	{
		vertexList[i].pos.x = (float)pVertexPos[i][0];
		vertexList[i].pos.y = (float)pVertexPos[i][1];
		vertexList[i].pos.z = (float)pVertexPos[i][2];
	}

	//ポリゴンの数だけループ
	for (int i = 0; i < _polygonCount; i++)
	{
		int startIndex = pMesh->GetPolygonVertexIndex(i);
		for (int j = 0; j < 3; j++)
		{
			int index = pMesh->GetPolygonVertices()[startIndex + j];

			FbxVector4 Normal;
			pMesh->GetPolygonVertexNormal(i, j, Normal);

			vertexList[index].normal =
				D3DXVECTOR3((float)Normal[0], (float)Normal[1], (float)Normal[2]);

			FbxVector2 uv = pMesh->GetLayer(0)->GetUVs()->GetDirectArray().GetAt(index);	//UV情報を取得
			vertexList[index].uv = D3DXVECTOR2((float)uv.mData[0], (float)(1.0 - uv.mData[1]));
		}
	}

	g.pDevice->CreateVertexBuffer(sizeof(Vertex)*_vertexCount, 0,
		D3DFVF_XYZ | D3DFVF_NORMAL | D3DFVF_TEX1, D3DPOOL_MANAGED,
		&_vertexBuffer, 0);

	Vertex *vCopy;
	_vertexBuffer->Lock(0, 0, (void**)&vCopy, 0);
	memcpy(vCopy, vertexList, sizeof(Vertex)*_vertexCount);
	_vertexBuffer->Unlock();

	delete[] vertexList;	//解放

	_indexBuffer = new IDirect3DIndexBuffer9*[_materialCount];	//インデックス情報を読み込む前に配列にする
	_polygonCountOfMaterial = new int[_materialCount];	//ポリゴン数を入れる変数を配列にする

	//インデックス情報を読み込み、インデックスバッファを作成する部分をループ
	for (int i = 0; i < _materialCount; i++)
	{

		int* indexList = new int[_indexCount];	//インデックス情報を入れる配列を作る

		int count = 0;
		for (int polygon = 0; polygon < _polygonCount; polygon++)
		{
			int materialID = pMesh->GetLayer(0)->GetMaterials()->GetIndexArray().GetAt(polygon);
			if (materialID == i)
			{
				for (int vertex = 0; vertex < 3; vertex++)
				{
					indexList[count++] = pMesh->GetPolygonVertex(polygon, vertex);
				}
			}
		}
		_polygonCountOfMaterial[i] = count / 3;

		//インデックスバッファを作成
		g.pDevice->CreateIndexBuffer(sizeof(int)* _indexCount, 0,
			D3DFMT_INDEX32, D3DPOOL_MANAGED, &_indexBuffer[i], 0);
		DWORD *iCopy;
		_indexBuffer[i]->Lock(0, 0, (void**)&iCopy, 0);
		memcpy(iCopy, indexList, sizeof(int)* _indexCount);
		_indexBuffer[i]->Unlock();
		delete[] indexList;
	}
}

void Fbx::RayCast(RayCastData *data)
{

	data->hit = FALSE;
	data->dist = 99999.0f;

	//頂点バッファをロック
	Vertex *vCopy;
	_vertexBuffer->Lock(0, 0, (void**)&vCopy, 0);




	//マテリアル毎ループ
	for (DWORD i = 0; i < _materialCount; i++)
	{
		//インデックスバッファをロック
		DWORD *iCopy;
		_indexBuffer[i]->Lock(0, 0, (void**)&iCopy, 0);


		//そのマテリアルのポリゴン毎
		for (DWORD j = 0; j < _polygonCountOfMaterial[i]; j++)
		{
			//3頂点
			D3DXVECTOR3 ver[3];
			ver[0] = vCopy[iCopy[j * 3 + 0]].pos;
			ver[1] = vCopy[iCopy[j * 3 + 1]].pos;
			ver[2] = vCopy[iCopy[j * 3 + 2]].pos;

			BOOL  hit;
			float dist;

			hit = D3DXIntersectTri(&ver[0], &ver[1], &ver[2],
				&data->start, &data->dir, NULL, NULL, &dist);

			//レイが一番近いポリゴンに当たったら
			if (hit && dist < data->dist)
			{
				data->hit = TRUE;
				data->dist = dist;

				D3DXVECTOR3 v1 = ver[2] - ver[0];	//
				D3DXVECTOR3 v2 = ver[2] - ver[1];	//
				D3DXVec3Cross(&data->normal, &v1, &v2);	//構造体にあるnormalにv1とv2の外積の結果を入れる
				D3DXVec3Normalize(&data->normal,&data->normal);	//ベクトルの正規化
			}


		}


		//インデックスバッファ使用終了
		_indexBuffer[i]->Unlock();
	}

	//頂点バッファ使用終了
	_vertexBuffer->Unlock();

}

