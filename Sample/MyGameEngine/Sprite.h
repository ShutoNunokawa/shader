#pragma once
#include "Node.h"
class Sprite :public Node
{
	LPD3DXSPRITE _pSprite;	//スプライト
	LPDIRECT3DTEXTURE9 _pTexture;	//テクスチャ

	//D3DXVECTOR3はfloat型のx,y,zをメンバに持つ構造体

	MyRect	_cut;			//切り抜き範囲


public:
	//-----------------------------------------
	//機能 : コンストラクタ
	//引数 : なし
	//戻値 : なし
	//-----------------------------------------
	Sprite();

	//-----------------------------------------
	//機能 : デストラクタ
	//引数 : なし
	//戻値 : なし
	//-----------------------------------------
	~Sprite();

	//ロード時間短縮のため一つの画像に複数の画像をまとめるための処理(static)
	static Sprite* Create(LPCSTR fileName, MyRect cut = MyRect(-999, -999, -999, -999));

	//-----------------------------------------
	//機能 : 画像読み込み
	//引数 :  fileName : 画像の名前	 cut : 切り抜き範囲
	//戻値 : なし
	//-----------------------------------------
	void Load(LPCSTR fileName, MyRect cut = MyRect(-999,-999,-999,-999));//第2引数を省略した場合画像全体が表示されるようにするために-999を入れる

	//-----------------------------------------
	//機能 : 描画
	//引数 : なし
	//戻値 : なし
	//-----------------------------------------
	void Draw();	//描画

	//各セッター	引数にデフォルトの値を設定することにより値の入力を省略することができる

	//void SetAnchorPoint(float x, float y, float z = 0);	//_anchorPointの値を入れる関数
	//void SetPosition(float x, float y, float z = 0);	//_positionの値を入れる関数
	//void SetRotate(float x, float y, float z );		//_rotateの値を入れる関数
	//void SetRotate(float z);						//z軸のみ回転
	//void SetScale(float x, float y, float z = 0);		//_scaleの値を入れる関数
	void SetRect(MyRect cut);	//切り抜き範囲変更用セッター

	//各ゲッター
	/*D3DXVECTOR3 GetSize();
	D3DXVECTOR3 GetAnchorPoint();
	D3DXVECTOR3 GetPosition();
	D3DXVECTOR3 GetRotate();
	D3DXVECTOR3 GetScale();*/

};

