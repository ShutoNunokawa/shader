#pragma once
#include <d3dx9.h>

class MyRect
{
public:
	float _left;
	float _top;
	float _width;
	float _height;

	MyRect();
	MyRect(float left, float top, float width, float height);
	~MyRect();

	bool IntersectsRect(MyRect targetRect);		//衝突判定用関数(ノードとノード)
	bool ContainsPoint(D3DXVECTOR3 targetPos);	//衝突判定用関数(ノードと点)
};

