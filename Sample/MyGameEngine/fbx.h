#pragma once
#include "Node.h"

//FBXファイルを使うためには
#include <fbxsdk.h>

#pragma comment(lib,"libfbxsdk-mt.lib")
//この2つが必要

//レイキャスト(レイを飛ばすときに必要な情報)用の構造体にまとめる
struct RayCastData
{
	D3DXVECTOR3 start;		//レイの発射点を入れる変数
	D3DXVECTOR3 dir;		//レイの向きを入れる変数
	BOOL		hit;		//当たったかどうかを入れる変数
	FLOAT		dist;		//距離
	D3DXVECTOR3 normal;		//当たった面の法線を入れる変数
};




class Fbx : public Node
{
	struct Vertex
	{
		D3DXVECTOR3 pos;
		D3DXVECTOR3 normal;
		D3DXVECTOR2 uv;
	};
	
	LPD3DXEFFECT _effect;

	//FBXファイルを開くために
	//FBX機能を管理するマネージャー,
	//ファイルを開くインポーター,
	//開いたファイルを管理するシーンを用意する
	FbxManager*  _manager;			//コンストラクタで初期化
	FbxImporter* _importer;			//コンストラクタで初期化
	FbxScene*    _scene;			//コンストラクタで初期化

	int _vertexCount;	//頂点の数を入れておくメンバ変数
	int _polygonCount;	//ポリゴン数を入れる変数
	int _indexCount;	//インデックス数を入れる変数
	int _materialCount;	//マテリアルを入れる変数(配列)
	int* _polygonCountOfMaterial;	//マテリアルごとのポリゴン数を記憶する変数

	LPDIRECT3DVERTEXBUFFER9 _vertexBuffer;		//コンストラクタで初期化
	LPDIRECT3DINDEXBUFFER9*  _indexBuffer;		//コンストラクタで初期化
	D3DMATERIAL9*            _material;			//コンストラクタで初期化
	LPDIRECT3DTEXTURE9*      _pTexture;			//コンストラクタで初期化

	void CheckNode(FbxNode* pNode);		//ノードの内容を調べる関数
	void CheckMesh(FbxMesh* pMesh);		//頂点情報を作成する関数


public:
	Fbx();
	~Fbx();
	static Fbx* Create(LPCSTR fileName);
	void Load(LPCSTR fileName);
	void Draw();

	void RayCast(RayCastData *data);	//構造体の8つ分の変数のデータをポインタで指定する
};