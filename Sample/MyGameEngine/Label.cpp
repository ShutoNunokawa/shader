#include "Label.h"


//-----------------------------------------
//機能 : コンストラクタ
//引数 : なし
//戻値 : なし
//-----------------------------------------
Label::Label()
{
	_pFont = nullptr;	//初期化
}


//-----------------------------------------
//機能 : デストラクタ
//引数 : なし
//戻値 : なし
//-----------------------------------------
Label::~Label()
{
	SAFE_RELEASE(_pFont);		//削除
	// _pFont->Release();		
}

//-----------------------------------------
//機能 : 
//引数 : 第一引数:文字列	第二引数:フォントの種類		第三引数:フォントサイズ
//戻値 : なし
//-----------------------------------------
Label* Label::Create(LPCSTR chr1, LPCSTR fontName, int size)
{
	
	Label* pLabel = new Label();		//Labelオブジェクト作成
	//pLabel->SetString(chr1);
	pLabel->Load(size, fontName);		//pLabelの呼び出し
	pLabel->SetString(chr1);
	return pLabel;
}

void Label::Load(int size, LPCSTR fontName)
{
	//2番目の引数がフォントサイズ、最後から2番目の引数がフォントの種類
	D3DXCreateFont(g.pDevice, size, 0, FW_NORMAL, 1,
		FALSE, DEFAULT_CHARSET, OUT_DEFAULT_PRECIS, DEFAULT_QUALITY,
		DEFAULT_PITCH | FF_SWISS, fontName, &_pFont);
}

//-----------------------------------------
//機能 : 描画
//引数 : なし
//戻値 : なし
//-----------------------------------------
void Label::Draw()
{
	//描画範囲指定
	RECT rect;

	rect.left = _position.x - _size.x * _anchorPoint.x;		//ラベルの左端 = アンカーポイントの位置(x軸) - ラベルの幅 * アンカーポイント
	rect.top = _position.y - _size.y * _anchorPoint.y;		//ラベルの高さ = アンカーポイントの位置(y軸) - ラベルの高さ * アンカーポイント
	rect.right = _size.x + rect.left;						//ラベルの幅 + ラベルの左端
	rect.bottom = _size.y + rect.top;						//ラベルの高さ + ラベルの上の端
	//引数:無視、表示する文字列、表示する文字数(-1だと全部)、表示範囲、フォーマット(今回は指定範囲の上)、色
	_pFont->DrawTextA(NULL, _string, -1, &rect, DT_LEFT | DT_TOP, D3DCOLOR_ARGB(255, 255, 255, 255));
}

//-----------------------------------------
//機能 : 表示内容変更
//引数 : 表示文字列を入れておく変数
//戻値 : なし
//-----------------------------------------
void Label::SetString(LPCSTR str)
{
	strcpy_s(_string, str);	//_stringにstrの内容をコピーする

	RECT rect = { 0, 0, 0, 0 };
	_pFont->DrawText(NULL, _string, -1, &rect, DT_CALCRECT, NULL);
	_size.x = rect.right;
	_size.y = rect.bottom;

}

