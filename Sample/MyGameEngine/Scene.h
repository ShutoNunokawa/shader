#pragma once
#include "Global.h"
#include "Sprite.h"
#include "Label.h"
#include "fbx.h"
#include <vector>


//前方宣言	後から使うことを知らせる
class Camera;	
class Scene
{
	std::vector<Node*> _nodes;	//後からでも配列を追加できるようにする

protected:
	Camera* _camera;

public:
	Scene();						//コンストラクタ
	virtual ~Scene();				//デストラクタ	継承先のデストラクタを呼ぶために仮想関数にする
	void AddChild(Node* pNode);		//ノード追加の関数
	void Draw();					//描画用関数


	//Gameクラスから全てのシーンを呼ぶようにしているため純粋仮想関数にしている
	virtual void Init() = 0;		//純粋仮想関数で名前だけ登録(要オーバーライド)
	virtual void Update();		//純粋仮想関数で名前だけ登録(要オーバーライド)
	virtual void Input();		//純粋仮想関数で名前だけ登録(要オーバーライド)
	void RemoveChild(Node* pNode);		//子ノード削除

	class NodeSort
	{
	public:
		bool operator() (Node* a, Node* b)
		{
			if (a->GetDistance() > b->GetDistance())
			{
				return true;
			}
			return false;
		}
	};
	
};


