#pragma once

//インクルード
#include <Windows.h>
#include <d3dx9.h>
#include "MyRect.h"
#include "Input.h"

//正しいメモリ解放をするためにマクロを作成
#define SAFE_DELETE(X) if(X){ delete X; X = nullptr;}
#define SAFE_RELEASE(X) if(X){ X->Release(); X = nullptr;} 

//前方宣言
//Scene型のポインタを宣言可能
class Scene;
class Camera;
//グローバル変数
class Global
{
public:
	//定数
	const int WINDOW_WIDTH = 800;  //ウィンドウの幅
	const int WINDOW_HEIGHT = 600; //ウィンドウの高さ

	//変数
	LPDIRECT3DDEVICE9 pDevice; //Direct3Dデバイスオブジェクト	先頭がLPで始まるのはポインタなのでnullptrで初期化
	Scene* pScene;			
	Input* pInput;
	Camera* pCamera;

	void ReplaceScene(Scene* pNextScene);	//シーン切り替え

};
//グローバルオブジェクト(外部宣言)
extern Global g;