#include "MyRect.h"


//-----------------------------------------
//機能 : コンストラクタ
//引数 : なし
//戻値 : なし
//-----------------------------------------
MyRect::MyRect()
{
	_left = 0;			//「左」の初期化
	_top = 0;			//「上」の初期化
	_width = 0;			//「幅」の初期化
	_height = 0;		//「高さ」の初期化
}


//-----------------------------------------
//機能 : コンストラクタ
//引数 : 第一引数:	左, 第二引数: 上,  第三引数:  幅,  第四引数:  高さ
//戻値 : なし
//-----------------------------------------
MyRect::MyRect(float left, float top, float width, float height)
{
	_left = left;			//_leftにleftの値を代入
	_top = top;				//_topにtopの値を代入
	_width = width;			//_widthにwidthの値を代入
	_height = height;		//_heightにheightの値を代入
}

//-----------------------------------------
//機能 : デストラクタ
//引数 : なし
//戻値 : なし
//-----------------------------------------
MyRect::~MyRect()
{

}

bool MyRect::IntersectsRect(MyRect targetRect)
{
	// ((   てすとの左    < 画像の右 &&    てすとの右     > 画像の左  ) && (  てすとの上    < 画像の下 &&    てすとの下      > 画像の上))
	if ((targetRect._left < _width   && targetRect._width >  _left    ) && (targetRect._top < _height  && targetRect._height >   _top  ))
	{
		return true;
	}

	return false;
}

bool MyRect::ContainsPoint(D3DXVECTOR3 targetPos)
{
	if (_left + _width >= targetPos.x && _left < targetPos.x &&
		_top + _height > targetPos.y && _top <= targetPos.y)
	{
		return true;
	}
	return false;
}