#pragma once
#include <dinput.h>
#include "XInput.h"

#pragma comment(lib, "dxguid.lib")
#pragma comment(lib, "dInput8.lib")
#pragma comment(lib,"Xinput.lib")

#define MAX_CONTROLLER_NUM 4			//コントローラーの最大接続数

//マクロ
#define SAFE_RELEASE(X) if(X){ X->Release(); X = nullptr;} 

//スティック識別
enum STICK_ID
{
	THUMB_LX,		//左スティックの横方向
	THUMB_LY,		//左スティックの縦方向
	THUMB_RX,		//右スティックの横方向
	THUMB_RY,		//右スティックの縦方向
};
//
//トリガー識別
enum TRIGGER_ID
{
	LEFT_TRIGGER,		//左トリガー
	RIGHT_TRIGGER		//右トリガー
};



class Input
{
	LPDIRECTINPUT8 _pDInput;			//DirectX本体			先頭がLPで始まるのはポインタなのでnullptrで初期化
	LPDIRECTINPUTDEVICE8 _pKeyDevice;	//キーボードデバイス	先頭がLPで始まるのはポインタなのでnullptrで初期化
	LPDIRECTINPUTDEVICE8 _pMouseDevice;	//マウスデバイス		先頭がLPで始まるのはポインタなのでnullptrで初期化
	BYTE _keyState[256];				//キーボードのキーが押されているかどうかを入れておく変数
	BYTE _prevKeyState[256];			//前回のキー状況記憶変数
	

	DIMOUSESTATE2 _mouseState;		//マウスの状態を入れる構造体
	DIMOUSESTATE2 _prevMouseState;	
	POINT _mousePosition;

	XINPUT_STATE _controllerState;			//コントローラーの最大接続数を入れる変数

	XINPUT_STATE _prevControllerState[MAX_CONTROLLER_NUM];		//コントローラー最大接続数

	XINPUT_VIBRATION vibration;			//バイブレーション
	
	
	


public:
	Input();
	~Input();

	void Init(HWND hWnd);
	void Update();

	bool IsKeyPush(int keyCode);		//キーが押されたかどうか
	bool IsKeyTap(int keyCode);			//前回キーが押されてなく、今押されたかどうか
	bool IsKeyRelease(int keyCode);		//前回キーが押されていて、今押されていないかどうか



	bool IsMousePush(int buttonCode);		//マウスがクリックされたかどうか
	bool IsMouseTap(int buttonCode);		//マウスがクリックされた瞬間かどうか
	bool IsMouseRelease(int buttonCode);	//マウスのクリックが離れた瞬間かどうか

	float GetMouseX();	//マウス横方向
	float GetMouseY();	//マウス縦方向
	float GetMouseWheel();	//マウスのホイール
	void SetMousePosition(int x, int y);	//マウスの位置
	POINT GetMousePosition(int x, int y);		//マウスの位置取得



	bool IsPadPush(int buttonCode);		//コントローラーのボタンを押しているか
	bool IsPadTap(int buttonCode);		//コントローラーのボタンを押した瞬間かどうか
	bool IsPadRelease(int buttonCode, int padID = 0);	//コントローラーのボタンを離した瞬間かどうか
	float GetStick(STICK_ID stickID, int padID = 0);	//スティックの傾き取得
	float GetTrigger(TRIGGER_ID triggerID, int padID = 0);	//トリガーの押し込み具合取得
	void Vibration(float leftPower, float rightPower, int padID = 0);	//コントローラーの振動



};

