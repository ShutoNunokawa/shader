#include "Game.h"
#include "..\PlayScene.h"
//#include "Input.h"


//-----------------------------------------
//機能 : コンストラクタ
//引数 : なし
//戻値 : なし
//-----------------------------------------
Game::Game()
{
	_pD3d = nullptr;		//先頭がLPで始まるのはポインタなのでnullptrで初期化
	g.pInput = nullptr;		//先頭がLPで始まるのはポインタなのでnullptrで初期化
	g.pScene = nullptr;		//先頭がLPで始まるのはポインタなのでnullptrで初期化
	
}

//-----------------------------------------
//機能 : デストラクタ
//引数 : なし
//戻値 : なし
//-----------------------------------------
Game::~Game()
{
	//開放処理
	//delete g.pInput;
	//delete g.pScene;
	SAFE_DELETE(g.pInput);			//安全な削除
	SAFE_DELETE(g.pScene);			//安全な削除
	SAFE_RELEASE(g.pDevice);		//安全な解放
	SAFE_RELEASE(_pD3d);			//安全な解放
	//g.pDevice->Release();
	//_pD3d->Release();
	//delete _sp;
}

//-----------------------------------------
//機能 : 初期化処理
//引数 : ウィンドウハンドル
//戻値 : なし
//-----------------------------------------
HRESULT Game::Init(HWND hWnd)
{

	

	//Direct3Dオブジェクトの作成
	_pD3d = Direct3DCreate9(D3D_SDK_VERSION);

	//DIRECT3Dデバイスオブジェクトの作成
	D3DPRESENT_PARAMETERS d3dpp;		//専用の構造体
	ZeroMemory(&d3dpp, sizeof(d3dpp));	//中身を全部0にする
	d3dpp.BackBufferFormat = D3DFMT_UNKNOWN;
	d3dpp.BackBufferCount = 1;
	d3dpp.SwapEffect = D3DSWAPEFFECT_DISCARD;
	d3dpp.Windowed = TRUE;
	d3dpp.EnableAutoDepthStencil = TRUE;
	d3dpp.AutoDepthStencilFormat = D3DFMT_D16;
	d3dpp.BackBufferWidth = g.WINDOW_WIDTH;
	d3dpp.BackBufferHeight = g.WINDOW_HEIGHT;

	if (FAILED(_pD3d->CreateDevice(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, hWnd,
		D3DCREATE_HARDWARE_VERTEXPROCESSING, &d3dpp, &g.pDevice)))
	{
		MessageBox(NULL, "デバイスオブジェクトの生成に失敗しました", "エラー", MB_OK);
		return E_FAIL;
	}

	g.pScene = new PlayScene;
	g.pScene->Init();


	g.pInput = new class Input;
	g.pInput->Init(hWnd);

	//第一引数:設定項目	第二引数:設定値
	g.pDevice->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);			//アルファブレンド有効or無効
	g.pDevice->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);		//半透明部分の表示
	g.pDevice->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);	//半透明部分の表示

}

//-----------------------------------------
//機能 : 更新処理
//引数 : なし
//戻値 : なし
//-----------------------------------------
void Game::Update()
{
	//g.pSceneは今のシーンが入っている
	g.pScene->Update();	

}

//-----------------------------------------
//機能 : 入力処理
//引数 : なし
//戻値 : なし
//-----------------------------------------
void Game::Input()
{
	g.pInput->Update();

	//g.pSceneは今のシーンが入っている
	g.pScene->Input();

}


//-----------------------------------------
//機能 : 描画処理
//引数 : なし
//戻値 : なし
//-----------------------------------------
void Game::Draw()
{
	//画面クリア ZBUFFERは奥行きを正しく表示する
	g.pDevice->Clear(0, NULL, D3DCLEAR_TARGET | D3DCLEAR_ZBUFFER, D3DCOLOR_XRGB(0, 0, 255), 1.0f, 0);

	//描画開始
	g.pDevice->BeginScene();

	//ゲーム画面の描画
	/*_sp->Draw();*/
	g.pScene->Draw();

	//描画終了
	g.pDevice->EndScene();

	//スワップ
	g.pDevice->Present(NULL, NULL, NULL, NULL);
}