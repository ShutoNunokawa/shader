#pragma once
#include "Node.h"

class Camera;
class Quad : public Node
{
	struct Vertex
	{
		D3DXVECTOR3 pos;	//位置
		D3DXVECTOR3 normal;	//法線
		D3DXVECTOR3 uv;		//uv座標
	};
	LPDIRECT3DVERTEXBUFFER9 _vertexBuffer;		//バーテックスバッファ(頂点)	先頭がLPで始まるのはポインタなのでnullptrで初期化
	LPDIRECT3DINDEXBUFFER9 _indexBuffer;		//インデックスバッファ(インデックス情報を格納する)		先頭がLPで始まるのはポインタなのでnullptrで初期化
	D3DMATERIAL9	       _material;		//質感
	LPDIRECT3DTEXTURE9 _pTexture;	//テクスチャ	先頭がLPで始まるのはポインタなのでnullptrで初期化
	Camera* _camera;

public:
	Quad();		//コンストラクタ
	~Quad();	//デストラクタ
	static Quad* Create(LPCSTR fileName);
	void Load(LPCSTR fileName);	//読み込み
	void Draw();			//描画
	void SetCamera(Camera* camera){ _camera = camera; }
};

