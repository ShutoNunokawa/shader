#include "Scene.h"
#include "Camera.h"
#include <algorithm>

//-----------------------------------------
//機能 : コンストラクタ
//引数 : なし
//戻値 : なし
//-----------------------------------------
Scene::Scene()
{
	_camera = Camera::Create();		    //カメラクラス呼び出し
	this->AddChild(_camera);			//シーンへ追加
	_camera->Update();					//カメラの更新処理を呼ぶ
	g.pCamera = _camera;
	
}

//-----------------------------------------
//機能 : デストラクタ
//引数 : なし
//戻値 : なし
//-----------------------------------------
Scene::~Scene()
{
	//ノードの数だけループ
	for (int i = 0; i < _nodes.size(); i++)
	{
		//delete _nodes[i];
		SAFE_DELETE(_nodes[i]); 

	}
}

void Scene::Input()
{
	//描画枚数分ループ
	for (int i = 0; i < _nodes.size(); i++)
	{
		_nodes[i]->Input();	//_node[i]の中身をInput()に代入
	}
}

void Scene::Update()
{
	//描画枚数分ループ
	for (int i = 0; i < _nodes.size(); i++)
	{
		_nodes[i]->Update();	//_node[i]の中身をUpdate()に代入

		//GetIsSortがtrueなら
		if (_nodes[i]->GetIsSort())
		{
			D3DXVECTOR3 v = _camera->GetPosition() - _nodes[i]->GetPosition();		//vにカメラの位置 - ノードの位置の値を入れる
			_nodes[i]->SetDistance(D3DXVec3Length(&v));		//全ノードの距離を取る
		}
		else
		{
			_nodes[i]->SetDistance(999999999);		
		}
	}
	std::sort(_nodes.begin(),_nodes.end(), NodeSort());		//ソート

}



//-----------------------------------------
//機能 : ノード追加
//引数 : スプライト型のポインタ変数
//戻値 : なし
//-----------------------------------------
void Scene::AddChild(Node* pNode)
{
	pNode->SetParent(this);
	_nodes.push_back(pNode);	//引数で渡されたオブジェクトをvectorに追加

}


//-----------------------------------------
//機能 : 描画
//引数 : なし
//戻値 : なし
//-----------------------------------------
void Scene::Draw()
{
	//描画枚数分ループ
	for (int i = 0; i < _nodes.size(); i++)
	{
		_nodes[i]->Draw();	//_node[i]の中身をDraw()に代入
	}
}


//-----------------------------------------
//機能 : 引数でノードを受け取り自身が持つノードから削除する
//引数 : ノード情報を入れておく変数
//戻値 : なし
//-----------------------------------------
void Scene::RemoveChild(Node* pNode)
{
	//ノードの数だけループ
	for (int i = 0; i < _nodes.size(); i++)
	{
		if (pNode == _nodes[i])
		{
			SAFE_DELETE(_nodes[i]);
			_nodes.erase(_nodes.begin()+i);
			break;

		}
	}
}