#pragma once
#include "Node.h"

class Label :public Node
{
	LPD3DXFONT _pFont;		//先頭がLPで始まるのはポインタなのでnullptrで初期化
	TCHAR _string[256];		//表示する文字列

public:
	Label();
	~Label();
	static Label* Create(LPCSTR chr1, LPCSTR fontName,int size);	//第一引数:文字列	第二引数:フォントの種類	  第三引数:フォントサイズ
	void Load(int size, LPCSTR fontName);		//準備 第一引数:フォントサイズ	第二引数:フォントの種類
	void Draw();		//描画
	void SetString(LPCSTR str);	//後から表示する内容を変更できるようにするための関数
	
};

