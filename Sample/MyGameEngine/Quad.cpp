#include "Quad.h"
#include "Camera.h"


//-----------------------------------------
//機能 : コンストラクタ
//引数 : なし
//戻値 : なし
//-----------------------------------------
Quad::Quad()
{
	_vertexBuffer = nullptr;		//ポインタの初期化
	_indexBuffer = nullptr;			//ポインタの初期化
	_pTexture = nullptr;			//ポインタの初期化

}


//-----------------------------------------
//機能 : デストラクタ
//引数 : なし
//戻値 : なし
//-----------------------------------------
Quad::~Quad()
{
	SAFE_RELEASE(_vertexBuffer);	//安全な解放
	SAFE_RELEASE(_indexBuffer);		//安全な解放
	SAFE_RELEASE(_pTexture);		//安全な解放
	
}


//-----------------------------------------
//機能 : 
//引数 : なし
//戻値 : なし
//-----------------------------------------
Quad* Quad::Create(LPCSTR fileName)
{
	auto quad = new Quad();		//ポリゴンの実体作成
	quad->Load(fileName);		//ポリゴン読み込み
	return quad;			
}

//-----------------------------------------
//機能 : 読み込み
//引数 : なし
//戻値 : なし
//-----------------------------------------
void Quad::Load(LPCSTR fileName)
{

	//4頂点分のデータ(0,1,2,3番)
	Vertex vertexList[] = {
		D3DXVECTOR3(-1, 1, 0), D3DXVECTOR3(0, 0, -1),  D3DXVECTOR2(0,0),
		D3DXVECTOR3(1, 1, 0), D3DXVECTOR3(0, 0, -1),   D3DXVECTOR2(1,0),
		D3DXVECTOR3(1, -1, 0), D3DXVECTOR3(0, 0, -1),  D3DXVECTOR2(1,1),
		D3DXVECTOR3(-1, -1, 0), D3DXVECTOR3(0, 0, -1), D3DXVECTOR2(0,1),
	};

	ZeroMemory(&_material, sizeof(D3DMATERIAL9));
	_material.Diffuse.r = 1.0f;
	_material.Diffuse.g = 1.0f;
	_material.Diffuse.b = 1.0f;

	//バッファ確保
	g.pDevice->CreateVertexBuffer(sizeof(vertexList), 0, D3DFVF_XYZ | D3DFVF_NORMAL | D3DFVF_TEX1,
		D3DPOOL_MANAGED, &_vertexBuffer, 0);

	//配列のコピー
	Vertex *vCopy;
	_vertexBuffer->Lock(0, 0, (void**)&vCopy, 0);	//バッファをロックする
	memcpy(vCopy, vertexList, sizeof(vertexList));	//頂点情報コピー
	_vertexBuffer->Unlock();	//ロック解除

	int indexList[] = { 0, 2, 3, 0, 1, 2 };

	g.pDevice->CreateIndexBuffer(sizeof(indexList), 0, D3DFMT_INDEX32,D3DPOOL_MANAGED, &_indexBuffer, 0);
	DWORD *iCopy;
	_indexBuffer->Lock(0, 0, (void**)&iCopy, 0);
	memcpy(iCopy, indexList, sizeof(indexList));
	_indexBuffer->Unlock();

	D3DXCreateTextureFromFileEx(g.pDevice, fileName, 0, 0, 0, 0,
		D3DFMT_UNKNOWN, D3DPOOL_DEFAULT, D3DX_FILTER_NONE, D3DX_DEFAULT,NULL, NULL, NULL, &_pTexture);

}

//-----------------------------------------
//機能 : 描画
//引数 : なし
//戻値 : なし
//-----------------------------------------
void Quad::Draw()
{
	//表示したい頂点バッファとインデックスバッファを指定
	g.pDevice->SetStreamSource(0, _vertexBuffer, 0, sizeof(Vertex));
	g.pDevice->SetIndices(_indexBuffer);
	g.pDevice->SetMaterial(&_material);
	g.pDevice->SetTexture(0, _pTexture);

	g.pDevice->SetFVF(D3DFVF_XYZ | D3DFVF_NORMAL | D3DFVF_TEX1);	//頂点の所持情報
	


	//移動行列
	D3DXMATRIX trans;
	D3DXMatrixTranslation(&trans, _position.x, _position.y, _position.z);

	//回転行列
	D3DXMATRIX rotateX, rotateY, rotateZ;
	D3DXMatrixRotationX(&rotateX, D3DXToRadian(_rotate.x));
	D3DXMatrixRotationY(&rotateY, D3DXToRadian(_rotate.y));
	D3DXMatrixRotationZ(&rotateZ, D3DXToRadian(_rotate.z));

	//拡大縮小
	D3DXMATRIX scale;
	D3DXMatrixScaling(&scale, _scale.x, _scale.y, _scale.z);

	//ワールド行列
	D3DXMATRIX world = scale * rotateZ * rotateX * rotateY * trans;

	//_cameraにSetCameraで持ってきた情報があったら
	if (_camera)
	{
		//ワールド行列に_cameraのビルボードも追加
		world = scale * _camera->GetBillboard() * rotateZ * rotateX * rotateY * trans;
	}


	g.pDevice->SetTransform(D3DTS_WORLD, &world);	//行列セット

	g.pDevice->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0, 4, 0, 2);	//4は頂点の数	2はポリゴン数


	
}

