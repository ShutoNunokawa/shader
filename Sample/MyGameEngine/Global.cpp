#include "Global.h"
#include "Scene.h"

//-----------------------------------------
//機能 : シーン移動
//引数 : 
//戻値 : なし
//-----------------------------------------
void Global::ReplaceScene(Scene* pNextScene)
{

	//シーンの削除、新シーンの追加、初期シーンに表示
	//delete pScene;			//pSceneの中身を削除
	SAFE_DELETE(pScene);
	pScene = pNextScene;	//pSceneにpNextSceneの中身を代入
	pScene->Init();			//pSceneの中身をInit()に代入

}