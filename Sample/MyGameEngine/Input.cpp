#include "Input.h"
//#include "ClearScene.h"
//#pragma comment(lib,"XInput.lib")



//-----------------------------------------
//機能 : コンストラクタ
//引数 : なし
//戻値 : なし
//-----------------------------------------
Input::Input()
{
	//初期化
	//_pDInput = nullptr;		

	_pKeyDevice = nullptr;								//ポインタの初期化
	ZeroMemory(_keyState, sizeof(_keyState));			//メモリの初期化
	ZeroMemory(_prevKeyState, sizeof(_prevKeyState));	//メモリの初期化

	_pMouseDevice = nullptr;								//ポインタの初期化
	ZeroMemory(&_mouseState, sizeof(_mouseState));			//メモリの初期化
	ZeroMemory(&_prevMouseState, sizeof(_prevMouseState));	//メモリの初期化
	_mousePosition.x = _mousePosition.y = 0;

	//ZeroMemory(_controllerState, sizeof(_controllerState));
	ZeroMemory(_prevControllerState, sizeof(_prevControllerState));		//メモリの初期化

	ZeroMemory(&vibration, sizeof(XINPUT_VIBRATION));		//メモリの初期化
}


//-----------------------------------------
//機能 : デストラクタ
//引数 : なし
//戻値 : なし
//-----------------------------------------
Input::~Input()
{
	//解放処理
	//_pDInput->Release();		
	//_pKeyDevice->Release();
	//_pMouseDevice->Release();
	SAFE_RELEASE(_pKeyDevice);
	SAFE_RELEASE(_pMouseDevice);

	
}

//-----------------------------------------
//機能 : 初期化
//引数 : ウィンドウハンドル
//戻値 : なし
//-----------------------------------------
void Input::Init(HWND hWnd)
{
	//DirectInput本体を作成、アドレスが_pDInputに入る
	DirectInput8Create(GetModuleHandle(NULL), DIRECTINPUT_VERSION,
		IID_IDirectInput8, (void**)&_pDInput, NULL);

	//キーボード
	_pDInput->CreateDevice(GUID_SysKeyboard, &_pKeyDevice, NULL);						//キーボードデバイスオブジェクト作成
	_pKeyDevice->SetDataFormat(&c_dfDIKeyboard);										//デバイスの種類
	_pKeyDevice->SetCooperativeLevel(hWnd, DISCL_NONEXCLUSIVE | DISCL_BACKGROUND);		//強調レベル(優先度)の設定

	//マウス
	_pDInput->CreateDevice(GUID_SysMouse, &_pMouseDevice, NULL);						//マウスデバイスオブジェクト作成
	_pMouseDevice->SetDataFormat(&c_dfDIMouse2);										//デバイスの種類
	_pMouseDevice->SetCooperativeLevel(hWnd, DISCL_NONEXCLUSIVE | DISCL_BACKGROUND);	//強調レベル(優先度)の設定


}


//-----------------------------------------
//機能 : キーとマウスの状態を配列に入れる
//引数 : なし
//戻値 : なし
//-----------------------------------------
void Input::Update()
{
	//キーボード
	_pKeyDevice->Acquire();
	memcpy(_prevKeyState, _keyState, sizeof(_keyState));	//キー情報コピー
	_pKeyDevice->GetDeviceState(sizeof(_keyState), &_keyState);

	//マウス
	_pMouseDevice->Acquire();
	memcpy(&_prevMouseState, &_mouseState, sizeof(_mouseState));	//マウス情報コピー
	_pMouseDevice->GetDeviceState(sizeof(_mouseState), &_mouseState);

	//コントローラー
	//最大接続台数分ループ
	for (int i = 0; i < MAX_CONTROLLER_NUM; i++)
	{
		memcpy(&_prevControllerState, &_controllerState, sizeof(_controllerState));
		XInputGetState(0, &_controllerState);
		XInputSetState(0, &vibration);
	}

}


//-----------------------------------------
//機能 : キーが押されたかどうかの判定
//引数 : 押されたかどうかの結果を入れる変数
//戻値 : なし
//-----------------------------------------
bool Input::IsKeyPush(int keyCode)
{
	//_keyStateの右から8ビット目が1になっているかどうか
	if (_keyState[keyCode] & 1<<7)
	{
		return true;
	}
	return false;
}


//-----------------------------------------
//機能 : 「前回は押してないが、今は押してる」かどうかの判定
//引数 : 押されたかどうかの結果を入れる変数
//戻値 : なし
//-----------------------------------------
bool Input::IsKeyTap(int keyCode)
{
	//前回も今も押されていたら
	if (_prevKeyState[keyCode] & 1 << 7 && _keyState[keyCode] & 1 << 7)
	{
		return true;
	}
	return false;
}


//-----------------------------------------
//機能 : 「前回は押されているが、今は押してない」かどうかの判定
//引数 : 押されたかどうかの結果を入れる変数
//戻値 : なし
//-----------------------------------------
bool Input::IsKeyRelease(int keyCode)
{
	//前回は押されていて、今は押されていなかったら
	if (_prevKeyState[keyCode] & 1 << 7 && _keyState[keyCode] & 1 << 7)
	{
		return true;
	}
	return false;
}


//-----------------------------------------
//機能 : マウスが押されたかどうかの判定
//引数 : 押されたかどうかの結果を入れる変数
//戻値 : なし
//-----------------------------------------
bool Input::IsMousePush(int buttonCode)
{
	//_mouseState.rgbButton[0]の右から8ビット目が1になっていたら
	if (_mouseState.rgbButtons[buttonCode] & 0x80)
	{
		return true;
	}
	return false;
}

//-----------------------------------------
//機能 : マウスが押された瞬間かどうかの判定
//引数 : 押された瞬間かどうかの結果を入れる変数
//戻値 : なし
//-----------------------------------------
bool Input::IsMouseTap(int buttonCode)
{
	if (IsMousePush(buttonCode) && _prevMouseState.rgbButtons[buttonCode] & 0x80 == 0)
	{
		return true;
	}
	return false;
}

//-----------------------------------------
//機能 : マウスを離したかどうかの判定
//引数 : 離したかどうかの結果を入れる変数
//戻値 : なし
//-----------------------------------------
bool Input::IsMouseRelease(int buttonCode)
{
	if (!IsMousePush(buttonCode) && _prevMouseState.rgbButtons[buttonCode] & 0x80)
	{
		return true;
	}
	return false;
}

//-----------------------------------------
//機能 : マウスの横方向の処理
//引数 : なし
//戻値 : なし
//-----------------------------------------
float Input::GetMouseX()
{
	return _mouseState.lX;
}

//-----------------------------------------
//機能 : マウスの縦方向の処理
//引数 : なし
//戻値 : なし
//-----------------------------------------
float Input::GetMouseY()
{
	return _mouseState.lY;
}

//-----------------------------------------
//機能 : マウスのホイールの処理
//引数 : なし
//戻値 : なし
//-----------------------------------------
float Input::GetMouseWheel()
{
	return _mouseState.lZ;
}

//-----------------------------------------
//機能 : マウスの位置セット
//引数 : x→横	　y→縦
//戻値 : なし
//-----------------------------------------
void Input::SetMousePosition(int x, int y)
{
	_mousePosition.x = x;
	_mousePosition.y = y;
}

//-----------------------------------------
//機能 : マウスの位置取得
//引数 : x→横	　y→縦
//戻値 : なし
//-----------------------------------------
POINT Input::GetMousePosition(int x, int y)
{
	return _mousePosition;
}

//-----------------------------------------
//機能 : コントローラーのボタンを押したときの処理
//引数 : x→横	　y→縦
//戻値 : なし
//-----------------------------------------
bool Input::IsPadPush(int buttonCode)
{
	if (_controllerState.Gamepad.wButtons & buttonCode)
	{
		//g.ReplaceScene(new ClearScene);
		//vibration.wLeftMotorSpeed = 65535;	//左モーターの強さ
		//vibration.wLeftMotorSpeed = 65535;	//右モーターの強さ
		return true;
	}
	return false;
}

/*
//-----------------------------------------
//機能 : コントローラーのボタンを押した瞬間の処理
//引数 : x→横	　y→縦
//戻値 : なし
//-----------------------------------------
bool Input::IsPadTap(int buttonCode, int padID)
{
	if (_controllerState.Gamepad.wButtons && XINPUT_GAMEPAD_A == 0)
	{
		return true;
	}
	return false;
}
*/

/*
//-----------------------------------------
//機能 : コントローラーのボタンを離したときの処理
//引数 : x→横	　y→縦
//戻値 : なし
//-----------------------------------------
bool Input::IsPadRelease(int buttonCode, int padID)
{
	if (!_controllerState.Gamepad.wButtons && _prevControllerState[padID].Gamepad.wButtons & buttonCode)
	{
		return true;
	}
	return false;
}*/