#pragma once
#include "Global.h"


class Scene;		//前方宣言


//マクロ	
#define CREATE_FUNC( className )\
	static className* Create()\
{\
	className* p = new className; \
	p->Load(); \
	return p; \
}


class Node
{
protected:
	D3DXVECTOR3 _size;			//サイズ
	D3DXVECTOR3 _anchorPoint;	//アンカーポイント
	D3DXVECTOR3 _position;		//位置
	D3DXVECTOR3 _rotate;		//回転角度
	D3DXVECTOR3 _scale;			//拡大率
	Scene* _parent;				//親を入れる変数
	float _distance;
	bool _isSort;				//ソートをするかしないか

public:
	Node();
	virtual ~Node();		//継承先のデストラクタを呼ぶために仮想関数にする
	virtual void Draw(){};
	virtual void Input(){};
	virtual void Update(){};

	//各セッター	引数にデフォルトの値を設定することにより値の入力を省略することができる

	void SetAnchorPoint(float x, float y, float z = 0);	//_anchorPointの値を入れる関数
	void SetPosition(float x, float y, float z = 0);	//_positionの値を入れる関数
	void SetPosition(D3DXVECTOR3 v);					//オーバーロード
	void SetRotate(float x, float y, float z);		//_rotateの値を入れる関数
	void SetRotate(float z);						//z軸のみ回転
	void SetScale(float x, float y, float z = 1);		//_scaleの値を入れる関数
	void RemoveFromParent();						//ノード削除関数
	void SetParent(Scene* pNode);				//親をセットする関数
	void SetDistance(float d){ _distance = d; }		//距離を設定するための関数
	void SetIsSort(bool b){ _isSort = b; }			//木をソートするための関数
	

	//各ゲッター
	D3DXVECTOR3 GetSize();			//サイズ
	D3DXVECTOR3 GetAnchorPoint();	//アンカーポイント
	D3DXVECTOR3 GetPosition();		//位置
	D3DXVECTOR3 GetRotate();		//回転
	D3DXVECTOR3 GetScale();			//拡大・縮小
	float GetDistance(){ return _distance; }
	bool GetIsSort(){ return _isSort; }

	MyRect GetBoundingBox();	 //当たり判定用四角形
	BOOL IntersectsRect(MyRect targetRect);	//ぶつかってるかぶつかってないか判定用関数
	


};

