#include "Node.h"
#include "Scene.h"


//-----------------------------------------
//機能 : コンストラクタ
//引数 : なし
//戻値 : なし
//-----------------------------------------
Node::Node()
{

	_size = D3DXVECTOR3(200, 200, 0);				//画像の大きさ(x,y,z)
	_anchorPoint = D3DXVECTOR3(0.5, 0.5, 0);	//画像のアンカーポイント(x,y,z)
	_position = D3DXVECTOR3(0, 0, 0);			//画像の位置(x,y,z)
	_rotate = D3DXVECTOR3(0, 0, 0);				//画像の回転(x,y,z)
	_scale = D3DXVECTOR3(1, 1, 1);				//画像の倍率(x,y,z)

	_parent = nullptr;

	_isSort = false;
}


//-----------------------------------------
//機能 : デストラクタ
//引数 : なし
//戻値 : なし
//-----------------------------------------
Node::~Node()
{

}


//-----------------------------------------
//機能 : 画像のアンカーポイントをセット
//引数 : x:x軸	y:y軸	z:z軸
//戻値 : なし
//-----------------------------------------
void Node::SetAnchorPoint(float x, float y, float z)
{
	_anchorPoint.x = x;
	_anchorPoint.y = y;
	_anchorPoint.z = z;
}


//-----------------------------------------
//機能 : 画像の位置をセット
//引数 : x:x軸	y:y軸	z:z軸
//戻値 : なし
//-----------------------------------------
void Node::SetPosition(float x, float y, float z)
{
	_position.x = x;
	_position.y = y;
	_position.z = z;
}


void Node::SetPosition(D3DXVECTOR3 v)
{
	_position = v;
}

//-----------------------------------------
//機能 : 画像の角度をセット
//引数 : x:x軸	y:y軸	z:z軸
//戻値 : なし
//-----------------------------------------
void Node::SetRotate(float x, float y, float z)
{
	_rotate.x = x;
	_rotate.y = y;
	_rotate.z = z;
}


//-----------------------------------------
//機能 : 画像の大きさをセット
//引数 : x:x軸	y:y軸	z:z軸
//戻値 : なし
//-----------------------------------------
void Node::SetScale(float x, float y, float z)
{
	_scale.x = x;
	_scale.y = y;
	_scale.z = z;
}


//-----------------------------------------
//機能 : 回転角度をセット(Z軸のみ)
//引数 : z軸
//戻値 : なし
//-----------------------------------------
void Node::SetRotate(float z)
{
	_rotate.x = 0;
	_rotate.y = 0;
	_rotate.z = z;
}


//-----------------------------------------
//機能 : サイズの取得
//引数 : なし
//戻値 : 画像のサイズ
//-----------------------------------------
D3DXVECTOR3 Node::GetSize()
{
	_size.x = _size.x * _scale.x;
	_size.y = _size.y * _scale.y;
	_size.z = _size.z * _scale.z;

	return _size;
}


//-----------------------------------------
//機能 : アンカーポイントを取得
//引数 : なし
//戻値 : 画像のアンカーポイント
//-----------------------------------------
D3DXVECTOR3 Node::GetAnchorPoint()
{
	return _anchorPoint;
}


//-----------------------------------------
//機能 : 位置を取得
//引数 : なし
//戻値 : 画像の位置
//-----------------------------------------
D3DXVECTOR3 Node::GetPosition()
{
	return _position;
}


//-----------------------------------------
//機能 : 回転角度を取得
//引数 : なし
//戻値 : 画像の回転角度
//-----------------------------------------
D3DXVECTOR3 Node::GetRotate()
{
	return _rotate;
}


//-----------------------------------------
//機能 : 拡大率を取得
//引数 : なし
//戻値 : 画像の拡大率
//-----------------------------------------
D3DXVECTOR3 Node::GetScale()
{
	return _scale;
}


//-----------------------------------------
//機能 : 当たり判定
//引数 : なし
//戻値 : なし
//-----------------------------------------
MyRect Node::GetBoundingBox()
{
	MyRect rect;

	rect._left = _position.x - _size.x * _anchorPoint.x;	//ラベルの左端 = アンカーポイントの位置(x軸) - ラベルの幅 * アンカーポイント
	rect._top = _position.y - _size.y * _anchorPoint.y;		//ラベルの上 = アンカーポイントの位置(y軸) - ラベルの高さ * アンカーポイント
	rect._width = _size.x + rect._left;						//ラベルの右端= ラベルの幅 + ラベルの左端
	rect._height = _size.y + rect._top;						//ラベルの下 = ラベルの高さ + ラベルの上の端

		return rect;
}


//-----------------------------------------
//機能 : ノード削除
//引数 : なし
//戻値 : なし
//-----------------------------------------
void Node::RemoveFromParent()
{
	
	_parent->RemoveChild(this);

}


//-----------------------------------------
//機能 : 親ノード設定
//引数 : なし
//戻値 : なし
//-----------------------------------------
void Node::SetParent(Scene* pNode)
{
	_parent = pNode;
}

//-----------------------------------------
//機能 : 描画(何もしない)
//引数 : なし
//戻値 : なし
//-----------------------------------------
//void Node::Draw()
//{
//
//}