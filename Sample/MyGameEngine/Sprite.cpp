#include "Sprite.h"


//-----------------------------------------
//機能 : コンストラクタ
//引数 : なし
//戻値 : なし
//-----------------------------------------
Sprite::Sprite()
{
	_pSprite = nullptr;		//ポインタ_pSpriteの初期化
	_pTexture = nullptr;	////ポインタ_pTextureの初期化

	

}


//-----------------------------------------
//機能 : デストラクタ
//引数 : なし
//戻値 : なし
//-----------------------------------------
Sprite::~Sprite()
{
	SAFE_RELEASE(_pTexture);	//_pTextureを解放
	SAFE_RELEASE(_pSprite);		//_pSpriteを解放
	//_pTexture->Release();	
	//_pSprite->Release();	
}


//-----------------------------------------
//機能 : 画像ファイルのロード
//引数 : fileName :画像の名前	cut :
//戻値 : 
//-----------------------------------------
Sprite* Sprite::Create(LPCSTR fileName, MyRect cut)
{
	Sprite* pSprite = new Sprite();		//Spriteオブジェクト作成
	pSprite->Load(fileName, cut);		//Load関数の呼び出し
	return pSprite;						//アドレスを返す
}

//-----------------------------------------
//機能 : 画像読み込み
//引数 :  fileName : 画像の名前 cut : 切り抜き範囲
//戻値 : なし
//-----------------------------------------
void Sprite::Load(LPCSTR fileName, MyRect cut)
{
	//スプライトオブジェクト作成
	D3DXCreateSprite(g.pDevice, &_pSprite);

	//テクスチャオブジェクトの作成
	D3DXCreateTextureFromFileEx(g.pDevice, fileName, 0, 0, 0, 0, D3DFMT_UNKNOWN, D3DPOOL_DEFAULT, D3DX_FILTER_NONE, D3DX_DEFAULT, NULL, NULL, NULL, &_pTexture);

	//全体表示
	if (cut._left == -999)
	{
		//テクスチャのサイズを取得
		D3DSURFACE_DESC d3dds;
		_pTexture->GetLevelDesc(0, &d3dds);
		_size.x = d3dds.Width;		//テクスチャの幅を_size.xに代入
		_size.y = d3dds.Height;		//テクスチャの高さを_size.yに代入

		//切抜き範囲
		_cut = MyRect(0, 0,_size.x,_size.y);	//切り抜かない(画像全体表示)

	}
	//切抜き範囲指定
	else
	{
		//切抜き範囲
		_cut = cut;
		_size.x = _cut._width;		//_size.xに切抜き範囲の幅から左を引いた値を代入
		_size.y = _cut._height;		//_size.yに切抜き範囲の高さから上を引いた値を代入
	}
}


//-----------------------------------------
//機能 : 描画
//引数 : なし
//戻値 : なし
//-----------------------------------------
void Sprite::Draw()
{
	//移動行列
	D3DXMATRIX trans;
	D3DXMatrixTranslation(&trans, _position.x, _position.y, 0);

	//回転行列
	D3DXMATRIX rotate;
	D3DXMatrixRotationZ(&rotate, D3DXToRadian(_rotate.z));	//度をラジアンに変換

	//拡大縮小
	D3DXMATRIX scale;
	D3DXMatrixScaling(&scale, _scale.x, _scale.y, 1);

	//ワールド行列(移動、回転、拡大縮小)
	D3DXMATRIX world = scale * rotate * trans;


	
	//変換行列セット
	_pSprite->SetTransform(&world);

	//画像のアンカーポイント
	D3DXVECTOR3 anchor = D3DXVECTOR3(_size.x * _anchorPoint.x, _size.y - _size.y * _anchorPoint.y, 0);

	//切り抜き範囲  _cutには左,上,幅,高さが入っている
	RECT cut = { _cut._left, _cut._top, _cut._left + _cut._width, _cut._top + _cut._height }; //引数は(左,上,右(左+幅),下(上+高さ)

	//ゲーム画面の描画
	//BeginとEndの間で描画
	_pSprite->Begin(D3DXSPRITE_ALPHABLEND);
	_pSprite->Draw(_pTexture, &cut, &anchor, nullptr, D3DCOLOR_ARGB(255, 255, 255, 255));	//第2引数:切り抜き範囲の変数、第3引数:アンカーポイントの変数
	_pSprite->End();


}


