#include "Light.h"


//-----------------------------------------
//機能 : デストラクタ
//引数 : なし
//戻値 : なし
//-----------------------------------------
Light::~Light()
{

}

//-----------------------------------------
//機能 : ライトの設定
//引数 : なし
//戻値 : ライトのオブジェクト
//-----------------------------------------
Light* Light::Create()
{
	D3DLIGHT9 lightState;	//ライト専用構造体

	ZeroMemory(&lightState, sizeof(lightState));

	//平行光源(ディレクショナルライト)
	lightState.Type = D3DLIGHT_DIRECTIONAL;

	//右下奥方向
	lightState.Direction = D3DXVECTOR3(1, -1, 1);

	//拡散反射光
	lightState.Diffuse.r = 1.0f;	//赤
	lightState.Diffuse.g = 1.0f;	//緑
	lightState.Diffuse.b = 1.0f;	//青

	//環境光
	lightState.Ambient.r = 1.0f;
	lightState.Ambient.g = 1.0f;
	lightState.Ambient.b = 1.0f;

	g.pDevice->LightEnable(0, TRUE);	//ライトのスイッチON 第一引数はライトの番号

	Light* light = new Light;
	return light;
}
