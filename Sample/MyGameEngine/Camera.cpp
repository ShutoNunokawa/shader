#include "Camera.h"

//-----------------------------------------
//機能 : コンストラクタ
//引数 : なし
//戻値 : なし
//-----------------------------------------
Camera::Camera()
{
	_position = D3DXVECTOR3(0, 3, -5);
	_target = D3DXVECTOR3(0, 0, 0);
	_up = D3DXVECTOR3(0, 1, 0);

	_angle = 60;
	_aspect = (float)g.WINDOW_WIDTH / g.WINDOW_HEIGHT;
	_nearClip = 0.3f;
	_farClip = 1000.0f;


	D3DXMatrixIdentity(&_view);		//何もしない行列
	D3DXMatrixIdentity(&_proj);		//何もしない行列
}

//-----------------------------------------
//機能 : デストラクタ
//引数 : なし
//戻値 : なし
//-----------------------------------------
Camera::~Camera()
{

}


//-----------------------------------------
//機能 : カメラクラス
//引数 : なし
//戻値 : 
//-----------------------------------------
Camera* Camera::Create()
{
	//カメラクラスの作成
	Camera* pCamera = new Camera();
	return pCamera;

}

//-----------------------------------------
//機能 : 更新処理
//引数 : なし
//戻値 : なし
//-----------------------------------------
void Camera::Update()
{
	//ビュー行列作成
	D3DXMatrixLookAtLH(&_view, &_position, &_target, &_up);
	g.pDevice->SetTransform(D3DTS_VIEW, &_view);	//第一引数は行列の種類

	//プロジェクション行列作成
	D3DXMatrixPerspectiveFovLH(&_proj, D3DXToRadian(_angle), _aspect, _nearClip, _farClip);
	g.pDevice->SetTransform(D3DTS_PROJECTION, &_proj);	//第一引数は行列の種類

	//ビルボード行列作成
	D3DXMatrixLookAtLH(&_billboard, &D3DXVECTOR3(0, 0, 0), &(_target - _position), &_up);
	D3DXMatrixInverse(&_billboard, NULL, &_billboard);


}

//-----------------------------------------
//機能 : カメラ位置を変更できるようにする
//引数 : なし
//戻値 : なし
//-----------------------------------------
void SetTarget()
{
	
}

//-----------------------------------------
//機能 : 上はどっちかを変更できるようにする
//引数 : なし
//戻値 : なし
//-----------------------------------------
void SetUp()
{
	
}


//ビルボード用
D3DXMATRIX Camera::GetBillboard()
{
	return _billboard;
}