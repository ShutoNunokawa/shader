#pragma once
#include "Node.h"
class Camera : public Node
{
	D3DXVECTOR3 _target;	//向いている方向を入れる変数
	D3DXVECTOR3 _up;		//上はどっちかを入れる変数

	float _angle;		//画角(遠近感、ズームの変更)
	float _aspect;		//アスペクト比(画面の縦横の比率　通常:画面の幅 / 高さ)
	float _nearClip;		//近クリッピング(最近距離)
	float _farClip;		//遠クリッピング(最遠距離)

	D3DXMATRIX _view;	//ビュー行列を入れる変数
	D3DXMATRIX _proj;	//プロジェクション行列を入れる変数
	D3DXMATRIX _billboard;	//ビルボード行列を入れる変数
public:
	Camera();
	~Camera();
	static Camera* Create();	//カメラクラス(情報維持のためstatic)
	
	void Update();		//更新処理
	void SetTarget(D3DXVECTOR3 target){ _target = target; }
	void SetUp(D3DXVECTOR3 up){ _up = up; }

	D3DXMATRIX GetBillboard();		//ビルボード用
};

