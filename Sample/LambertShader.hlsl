//float4 main( float4 pos : POSITION ) : SV_POSITION
//{
//	return pos;
//}

// シェーダーは実行されてからコンパイルされる！

// グローバル変数
					// float4x4 : 行列を扱う変数
float4x4 matWorld;	// ワールド行列
float4x4 matView;	// ビュー行列
float4x4 matProj;	// プロジェクション行列
float4x4 matRotateX;	// 回転行列
float4x4 matRotateY;	// 回転行列
float4x4 matRotateZ;	// 回転行列

struct VS_OUT		// 頂点シェーダの出力用構造体
{
	float4 pos		: SV_POSITION;	// 位置
	float4 color	: COLOR;		// 色
};

// 頂点シェーダー
// float4 VS(float4 pos : POSITION, float4 normal : NORMAL) : SV_POSITION	
VS_OUT VS(float4 pos : POSITION, float4 normal : NORMAL)
												// : SV_POSITIONは既に構造体で宣言済みなので削除
												// 画面上でどの位置を表しているか
												// 返す値が座標であることを表すSV_POSITION
												// セマンティクスで戻り値の値を何にするかを決める
{
	//normal.x *= matRotateX;
	//normal.y *= matRotateY;
	//normal.z *= matRotateZ;


	VS_OUT outData;
	float4 outPos = pos;
	float4 outNormal = normal;

	//float outPos = pos + normal				// 位置+法線を足すとドーナツは太くなる
	//outPos.x += 2		回転の軸は変わらず座標が中心を軸に動く

	outNormal = mul(outNormal, matRotateX);		// X軸の法線回転
	outNormal = mul(outNormal, matRotateY);		// Y軸の法線回転
	outNormal = mul(outNormal, matRotateZ);		// Z軸の法線回転
	outPos = mul(outPos, matWorld);				// 出力用Position											// mul(乗算) Positionに	ワールド行列を乗算
	outPos = mul(outPos, matView);				//			 さらに		ビュー行列を乗算
	outPos = mul(outPos, matProj);				//			 さらに		プロジェクション行列を乗算
	//outPos.x += 2;	位置がずれた所で回転する
	outData.pos = outPos;
	normal = outNormal;

	float4 light = float4(-1, 1, -1, 0);		// 引数 x y z w
		light = normalize(light);				// ベクトルの正規化	normalize(正規化)
	outData.color = dot(normal, light);			// 内積を求める		dot()
	outData.color.a = 1;						// a(不透明度)も小さくなり、透明になってしまうので強制的に書き換え

	return outData;
}

// ピクセルシェーダー
//float4 PS(float4 pos : SV_POSITION) : COLOR	// 返す値が色なのでCOLOR
float4 PS(VS_OUT inData) : COLOR
												// 指す場所は頂点なのでSV_POSITIONが入る
{
	
	//if (pos.x < 400)
	//	return float4(1, 0, 0, 1);					// Red Green Blue Alpha(不透明度)
	//else
	//	return float4(0, 1, 0, 1);					// xが400より大きい場合は赤 xが400未満なら緑
	//return float4(0, 1, 0, pos.y / 600);			// 透明度を下に行けば行くほど濃くしていく

	return inData.color * float4(1, 0, 0, 1);		// 乗算すると色が出る！
	//return inData.color;
}

technique										// 状況によって使うシェーダーを分ける事ができる
{
	pass
	{
												// 後ろのVS PS は関数名
		VertexShader = compile vs_3_0 VS();		// 頂点シェーダー		Version3.0でコンパイルする指示
		PixelShader = compile ps_3_0 PS();		// ピクセルシェーダー	Version3.0でコンパイルする指示
	}
}