// グローバル変数
// float4x4 : 行列を扱う変数
float4x4 matWorld;	// ワールド行列
float4x4 matView;	// ビュー行列
float4x4 matProj;	// プロジェクション行列
//float4x4 matRotateX;	// 回転行列
//float4x4 matRotateY;	// 回転行列
//float4x4 matRotateZ;	// 回転行列
float4x4 matRotate;
//float4 vecCam;
float3 vecCam;			// 見る専用
float3 vecLightDir;		// ライトの向き
texture texDecal;

// テクスチャの設定(張り方等
// 補完 : ニアレストレイバー(境界をどちらかの色で埋める
//		: バイリニア補完(周りの色の平均を求めて埋める
sampler SamplerDecal = sampler_state
{
	Texture = <texDecal>;
	// ニアレストレイバーの場合はLINEARをPOINTに
	MipFilter = LINEAR;
	MinFilter = LINEAR;
	MagFilter = LINEAR;
};

struct VS_OUT		// 頂点シェーダの出力用構造体
{
	float4 pos		: SV_POSITION;	// 位置
	float4 normal	: NORMAL;		// 法線
	float4 eye		: TEXCOORD1;	// 視点情報
	float2 uv		: TEXCOORD0;
};

//VS_OUT VS(float4 pos : POSITION, float4 normal : NORMAL)
VS_OUT VS(float4 pos : POSITION, float3 normal : NORMAL, float2 uv : TEXCOORD0)
{
	VS_OUT outData;
	float4 outPos = pos;
		//	outNormal = mul(outNormal, matRotateX);		// X軸の法線回転
		//	outNormal = mul(outNormal, matRotateY);		// Y軸の法線回転
		//	outNormal = mul(outNormal, matRotateZ);		// Z軸の法線回転
		outPos = mul(outPos, matWorld);				// 出力用Position											// mul(乗算) Positionに	ワールド行列を乗算
	outPos = mul(outPos, matView);				//			 さらに		ビュー行列を乗算
	outPos = mul(outPos, matProj);				//			 さらに		プロジェクション行列を乗算
	outData.pos = outPos;

	//outData.normal = mul(normal,matRotate);	// wを0にする代わりに、法線normalをfloat3にして
	// float4で使うときにキャストする
	outData.normal = mul(float4(normal, 0), matRotate);
	//outData.normal.w = 0;

	float4 worldPos = mul(pos, matWorld);
		//outData.eye = normalize(vecCam - worldPos);
		outData.eye = normalize(float4(vecCam, 0) - worldPos);

	outData.uv = uv;

	return outData;
}

//ピクセルシェーダー
float4 PS(VS_OUT inData) :COLOR
{
	//float4 light = float4(1, -1, 1, 0);
	float4 light = float4(vecLightDir, 0);
	light = normalize(light);

	float4 NL = dot(inData.normal, -light);
		//float4 ref = normalize(2 * NL*inData.normal - light);
		float4 ref = reflect(light, inData.normal);

		float speculerSize = 3.0f;
	float speculerPower = 2.0f;

	float4 speculer = speculerPower*pow(saturate(dot(ref, inData.eye)), speculerSize);//鏡面反射率*光の強さ*(反射ベクトル・視線)^サイズ

		//float4 color = NL * float4(1, 0, 0, 1) + speculer;		//RGBA
		float4 color = NL * tex2D(SamplerDecal ,inData.uv) + speculer;
		color.a = 1;

	return color;

}

technique
{
	pass
	{
		VertexShader = compile vs_3_0 VS();
		PixelShader = compile ps_3_0 PS();
	}
}